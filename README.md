# README

This repository contains an examination of religious (terror) attacks. The dataset is hosted on [kaggle](https://www.kaggle.com/argolof/predicting-terrorism).

Originally the data was scraped from [The Religion of Peace](http://www.thereligionofpeace.com/) website.

# How to?

- The exploration is done in R and written in Rmarkdown. To run this file, you need R and RStudio.
- output is in html
